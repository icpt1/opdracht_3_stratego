#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BOARD_SIZE 10

typedef struct {
    int type;          // Nummer dat het type stuk representeert
    int isPlayerPiece; // 1 als het stuk van de speler is, anders 0
} Piece;

Piece *board[BOARD_SIZE][BOARD_SIZE];

void createPiece(int x, int y, int type, int isPlayerPiece) {
    board[x][y] = malloc(sizeof(Piece));
    board[x][y]->type = type;
    board[x][y]->isPlayerPiece = isPlayerPiece;
}


void initializeBoard() {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j] = NULL;
        }
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        if (i < ((BOARD_SIZE/2)-1)) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                createPiece(i, j, rand() % 10, 0);
            }
        }
        if (i > ((BOARD_SIZE/2))) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                createPiece(i, j, rand() % 10, 1);
            }
        }
    }
}

void freeBoard() {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (board[i][j] != NULL) {
                free(board[i][j]);
            }
        }
    }
}

void displayBoard() {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (board[i][j] != NULL) {
                if (!board[i][j]->isPlayerPiece) {
                    printf(" * ");  // Vijandige stukken
                } else {
                    printf(" %d ", board[i][j]->type);  // Eigen stukken
                }
            } else {
                printf(" . ");
            }
        }
        printf("\n");
    }
}

int isValidMove(int fromX, int fromY, int toX, int toY) {
    if (fromX < 0 || fromX >= BOARD_SIZE || fromY < 0 || fromY >= BOARD_SIZE ||
        toX < 0 || toX >= BOARD_SIZE || toY < 0 || toY >= BOARD_SIZE) {
        return 0;
    }

    if (board[fromX][fromY] == NULL || board[toX][toY] != NULL) {
        return 0;
    }

    if (!((fromX == toX && abs(fromY - toY) == 1) || (fromY == toY && abs(fromX - toX) == 1))) {
        return 0;
    }

    // Controleer of de startpositie een stuk van de speler bevat
    if (board[fromX][fromY] == NULL || board[fromX][fromY]->isPlayerPiece != 1) {
        return 0;
    }


    return 1;
}

void movePiece(int fromX, int fromY, int toX, int toY) {
    if (isValidMove(fromX, fromY, toX, toY)) {
        board[toX][toY] = board[fromX][fromY];
        board[fromX][fromY] = NULL;
    } else {
        printf("Ongeldige beweging!\n");
    }
}

int main() {
    initializeBoard();

    int x, y;
    char direction[10];

    while (1) {
        displayBoard();
        printf("Kies een stuk om te verplaatsen (X Y coördinaten): ");
        scanf("%d %d", &x, &y);

        printf("Kies een richting om te bewegen (up/down/left/right): ");
        scanf("%s", direction);

        int toX = x, toY = y;
        if (strcmp(direction, "up") == 0) toX--;
        else if (strcmp(direction, "down") == 0) toX++;
        else if (strcmp(direction, "left") == 0) toY--;
        else if (strcmp(direction, "right") == 0) toY++;
        else {
            printf("Ongeldige richting!\n");
            continue;
        }

        movePiece(x, y, toX, toY);
    }

    freeBoard();
    return 0;
}