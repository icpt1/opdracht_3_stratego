cmake_minimum_required(VERSION 3.26)
project(opdracht_3_stratego C)

set(CMAKE_C_STANDARD 11)

add_executable(opdracht_3_stratego main.c)
